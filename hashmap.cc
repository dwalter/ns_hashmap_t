/*
  hashmap.cc
  Example:  source for  non-stop  wait-free / weight-less [low fat]
  locking using extra bits in 64bit pointer for reference counter or
  data container pointer
      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#include <ns_hashmap_t/ns_hashmap_t.h>
#include <fstream>
#include <signal.h>

typedef ns::ns_hashmap_t< uint32_t, uint32_t >  map_t ;
typedef map_t::link_ptr               link_ptr ;
typedef map_t::wrapper_t              wrapper_t ;
const uint32_t MAX=1000 ;
namespace ns
{
  typedef std::string text ;
} 
void* start( void* arg )
{
  map_t& map( * static_cast< map_t* >( arg ) ) ;

  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    map.insert( i, i ) ;
  }
  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    try
    {
      wrapper_t node( map.get( i ) ) ;
    }
    catch( std::exception& e )
    {
      std::cerr << e.what() << std::endl ;
    }
  }
  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    try
    {
      wrapper_t node( map.remove( i ) ) ;
    }
    catch( std::exception& e )
    {
      std::cerr << e.what() << std::endl ;
    }
  }
  pthread_exit(0) ;
}

void* insert( void* arg )
{
  map_t& map( * static_cast< map_t* >( arg ) ) ;
  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    map.insert( i, i ) ;
  }
  pthread_exit(0) ;
}

void* get( void* arg )
{
  map_t& map( * static_cast< map_t* >( arg ) ) ;
  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    try
    {
      wrapper_t node( map.get( i ) ) ;
      if ( ! node || *node != i )
      {
	std::cerr << "missing[" << i << "]" << std::endl ;
      }
    }
    catch( std::exception& e )
    {
      std::cerr << e.what() << std::endl ;
    }
  }
  pthread_exit(0) ;
}

void* remove( void* arg )
{
  map_t& map( * static_cast< map_t* >( arg ) ) ;
  for ( uint32_t i = 0 ; i < MAX ; i++ )
  {
    try
    {
      wrapper_t node( map.remove( i ) ) ;
    }
    catch( std::exception& e )
    {
      std::cerr << e.what() << std::endl ;
    }
  }
  pthread_exit(0) ;
}

void handler       ( int signal )
{
  TRACE ;
  std::cerr << "signal[" << sys_siglist[signal] << std::endl ;
}

void* handler_thread( void* )
{
  TRACE ;
  sigset_t set;
  sigfillset( &set ) ;
  struct sigaction action;
  action.sa_flags = SA_SIGINFO;
  action.sa_handler = handler ;
  sigaction(SIGALRM, &action, NULL);
  sigaddset( &set, SIGUSR1 ) ;
  sigaddset( &set, SIGUSR2 ) ;
  sigaddset( &set, SIGTERM ) ;
  sigaddset( &set, SIGQUIT ) ;
  sigaddset( &set, SIGHUP  ) ;
  sigaddset( &set, SIGFPE  ) ;
  sigaddset( &set, SIGILL  ) ;
  sigaddset( &set, SIGSEGV ) ;
  sigaddset( &set, SIGBUS  ) ;
  sigaddset( &set, SIGABRT ) ;
  sigaddset( &set, SIGTRAP ) ;
  sigaddset( &set, SIGSYS  ) ;
  sigaddset( &set, SIGINT ) ;
  sigaddset( &set, SIGPIPE ) ;

  sigaction( SIGALRM, &action, 0 );
  sigaction( SIGUSR1, &action, 0 ) ;
  sigaction( SIGUSR2, &action, 0 ) ;
  sigaction( SIGTERM, &action, 0 ) ;
  sigaction( SIGQUIT, &action, 0 ) ;
  sigaction( SIGHUP , &action, 0 ) ;

  sigaction( SIGFPE , &action, 0 ) ;
  sigaction( SIGILL , &action, 0 ) ;
  sigaction( SIGSEGV, &action, 0 ) ;
  sigaction( SIGBUS , &action, 0 ) ;
  sigaction( SIGABRT, &action, 0 ) ;
  sigaction( SIGTRAP, &action, 0 ) ;
  sigaction( SIGSYS , &action, 0 ) ;
  sigaction( SIGINT , &action, 0 ) ;
  sigaction( SIGPIPE, &action, 0 ) ;

  pthread_sigmask(SIG_UNBLOCK, &set, NULL);

  while( true )
  {
    timespec ts = {1,0} ;
    nanosleep( & ts, 0 ) ;
  }
  return 0 ;
}

int main( int argc, char** argv )
{
#define PRINT(x) do { std::cerr << std::setw( 32 ) << std::left << #x << "[" << x << "]" << std::endl ; } while(0)

  try
  {
    map_t map( 4096 ) ;
    std::cerr << "MAX_REFERENCES[" << ns::MAX_REFERENCES << "]" << std::endl ;
    const int max_threads( getenv( "THREADS" ) ? atoi( getenv( "THREADS" ) ) : 2 ) ;
    pthread_t thread[max_threads+1] ;

    pthread_attr_t attr;
    size_t stacksize;

    pthread_attr_init(&attr);
    // pthread_attr_getstacksize(&attr, &stacksize); 
    stacksize = 1 << 16 ;
    pthread_attr_setstacksize(&attr, stacksize); 

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_create( &thread[i], &attr, &start, reinterpret_cast<void*>( & map ) ) ;
    }

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    PRINT( map.inserts ) ;
    PRINT( map.gets ) ;
    PRINT( map.removes ) ;
    PRINT( map.reserves ) ;

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_create( &thread[i], &attr, &insert, reinterpret_cast<void*>( & map ) ) ;
    }

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    PRINT( map.inserts ) ;
    PRINT( map.gets ) ;
    PRINT( map.removes ) ;
    PRINT( map.reserves ) ;

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_create( &thread[i], &attr, &get, reinterpret_cast<void*>( & map ) ) ;
    }

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    PRINT( map.inserts ) ;
    PRINT( map.gets ) ;
    PRINT( map.removes ) ;
    PRINT( map.reserves ) ;

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_create( &thread[i], &attr, &remove, reinterpret_cast<void*>( & map ) ) ;
    }

    for ( int i=0; i < max_threads; i++ )
    {
      pthread_join( thread[i], 0 ) ;
    }
    PRINT( map.inserts ) ;
    PRINT( map.gets ) ;
    PRINT( map.removes ) ;
    PRINT( map.reserves ) ;
  }
  catch( std::exception& e )
  {
    std::cerr << e.what() << std::endl ;
  }
  PRINT( ns::lockable_pointer_t<int>::instances() ) ;
  PRINT( ns::lockable_pointer_t<int>::destroyed() ) ;
  PRINT( ns::lockable_wrapper_t<int>::instances() ) ;
  PRINT( ns::lockable_wrapper_t<int>::destroyed() ) ;
  PRINT( map_t::link_t::instances() ) ;
  PRINT( map_t::link_t::destroyed() ) ;
  PRINT( map_t::link_t::deleted() ) ;
  std::ofstream o( "done" ) ;
  o << "done" << std::endl ;
  return 0 ;
}
