/*
  ns_lockable_wrapper_t.h :
  Example:  source for  non-stop  wait-free / weight-less [low fat]
  locking using extra bits in 64bit pointer for reference counter or
  data container pointer
      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NS_LOCKABLE_WRAPPER_T_H
#define NS_LOCKABLE_WRAPPER_T_H
#include <ns_hashmap_t/ns_lockable_pointer_t.h>
#ifndef NS_LOCKABLE_T_H
#error Do not #include <ns_hashmap_t/ns_lockable_wrapper_t.h> without
#error #include <ns_hashmap_t/ns_lockable_pointer_t.h>
#endif
#include <stdexcept>

namespace ns
{
  template < typename type_t >
  struct lockable_wrapper_t: lockable_pointer_t< type_t >
  {
    typedef lockable_pointer_t< type_t > lockable_type_t ;

    static uint32_t& instances()
    {
      static uint32_t counter = 0 ;
      return counter ;
    }

    static uint32_t& destroyed()
    {
      static uint32_t counter = 0 ;
      return counter ;
    }
	
    // lockable_type_t lockable ;
    bool locked ;

    lockable_wrapper_t( type_t* rhs=nullptr ):
      lockable_type_t( rhs )
    , locked( lockable_type_t::lockable ? true : false )
    {
      if ( lockable_type_t::lockable )
      {
        __sync_fetch_and_add( & instances(), 1 ) ; ;
        lockable_type_t::inc() ;
      }
    }

    lockable_wrapper_t( lockable_type_t&& rhs ):
      lockable_type_t( rhs )
    , locked( rhs )
    {
    }

    lockable_wrapper_t( lockable_type_t& rhs ):
      lockable_type_t( rhs )
    , locked( false )
    {
      if ( lockable_type_t::lockable  )
      {
        __sync_fetch_and_add( & instances(), 1 ) ; ;
        lockable_type_t::inc() ;
        locked = true ;
      }
    }

    lockable_wrapper_t( lockable_wrapper_t&& rhs ):
      lockable_type_t( rhs )
    , locked( rhs.locked ) 
    {
      rhs.locked = false ;
    }

    lockable_wrapper_t( lockable_wrapper_t& rhs ):
      lockable_type_t( rhs )
    , locked( false )
    {
      if ( lockable_type_t::lockable )
      {
        __sync_fetch_and_add( & instances(), 1 ) ; ;
        lockable_type_t::inc() ;
	locked = true ;
      }
    }

    const lockable_wrapper_t& operator=( lockable_wrapper_t&& rhs )
    {
      lockable_type_t::lockable = rhs.lockable ;
      locked   = rhs.lockable ;
      rhs.locked = false ;
      return *this ;
    }

    const lockable_wrapper_t& operator=( lockable_wrapper_t& rhs )
    {
      lockable_type_t::operator=( rhs ) ;
      if ( lockable_type_t::lockable )
      {
        locked   = true ;
        lockable_type_t::inc() ;
      }
      return *this ;
    }

    const lockable_wrapper_t& operator=( lockable_type_t& rhs )
    {
      lockable_type_t::operator=( rhs ) ;
      if ( lockable_type_t::lockable )
      {
        locked   = true ;
        lockable_type_t::inc() ;
      }
      return *this ;
    }

    ~lockable_wrapper_t()
    {
      if ( lockable_type_t::lockable && locked )
      {
        lockable_type_t::dec() ;
        if ( lockable_type_t::references() == 0 &&
           ! lockable_type_t::locked() )
        {
          __sync_fetch_and_add( & destroyed(), 1 ) ; ;
          lockable_type_t::lock() ;
          lockable_type_t::destroy() ;
        }
      }
    }

    uint32_t references()
    {
      return
        lockable_type_t::lockable ?
        lockable_type_t::lockable->references : 0 ;
    }

    void swap( lockable_wrapper_t&& rhs )
    {
      std::swap( lockable_type_t::lockable, rhs.lockable ) ;
      std::swap( locked           , rhs.locked ) ;
    }

    void take( lockable_wrapper_t& rhs )
    {
      lockable_type_t::lockable = rhs.lockable ;
      locked                    = rhs.locked   ;
      rhs.lockable              = nullptr ;
      rhs.locked                = false ;
    }

    // operator bool()      const{ return  lockable_type_t::lockable ; }
    // bool operator !()    const{ return !lockable_type_t::lockable ; }
    // type_t& operator*()       { return *lockable_type_t::pointer() ; }
    // type_t* operator->()      { return  lockable_type_t::pointer() ; }
  } ;
}
#endif
