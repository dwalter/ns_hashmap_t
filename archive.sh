#!/bin/bash
function setversion
{
    local all=""
    local minor=""
    local major=""
    if [[ ! -e .version ]]; then
	v=00.00
    else
	v=$(cat .version)
	all=${v};
	minor=${all#*.};
	major=${all%.*}
	major=${major#0*}
	minor=${minor#0*}
	minor=$(( minor+1 ))
    fi
    if (( minor > 99 )); then
	minor=0;
	major=$(( major+1 )) ;
    fi
    printf "%02d.%02d\n" ${major} ${minor} > .version
    cat .version
}

function settag
{
    setversion
    version=$(cat .version)
    message="$(git log --pretty=format:%s -n1)"
    git tag v${version} -a -m "${message}"
    git checkout -b v${version}
}

function deletetag
{
    tag=v$(cat .version) ;
    git co master ;
    git branch -D ${tag} ;
    git tag -d ${tag} ;
}

function git-sync
{
    local push=0
    local pull=0
    for arg in $*; do
	case ${arg} in
	    --push)
		shift ;
		push=1
		break;
		;;
	    --pull)
		shift ;
		pull=1
		break;
		;;
	    --tag)
		settag
		exit 0 ;
		break;
		;;
	    --deletetag)
		deletetag;
		exit 0 ;
		break;
		;;
	    *)
		break 
		;;
	esac
    done

    if (( ! push && ! pull && ! tag )); then
	echo "Usage git-sync [ --push commit message ] [ --pull ]"
	return 1
    fi

    if (( tag )); then
	settag
	exit 0
    fi

    if (( push )); then
	setversion
	version=$(cat .version)
	git add .version
	make clean
#	make git-add
	branch=$(git branch -l|grep "*"|cut -f2 -d' ')
	message="$(git log --pretty=format:%s -n1)"
#	git tag v${version} -a -m "${message}"
	git commit -am "${message}" --amend
	git repack -Aad
	git checkout -b v${version}
	git co ${branch}
#	git commit --amend -a -m "${message}" 
	make tar
	destination=${PWD##*/}
	rsync -ralcz ${PWD}/ ~/Dropbox/sync/src/${destination}/
	return $? ;
    fi

    if (( pull )); then
	destination=${PWD##*/}
	rsync -ralcz ~/Dropbox/sync/src/${destination}/ ${PWD}/ 
	return $? ;
    fi
}

git-sync ${*}
