/*
  ns_stack_t.h : example source for non-stop wait-free/weight-less
  [ low fat ] locking using extra bits in 64bit pointer
      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/
#ifndef NS_STACK_H
#define NS_STACK_H
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <type_traits>
#define FULL_MEMORY_BARRIER __asm__ __volatile__("" : : : "memory" )
// #define FULL_MEMORY_BARRIER __sync_synchronize()
#include <ns_hashmap_t/ns_lock_t.h>
#include <ns_hashmap_t/ns_lockable_t.h>
#include <ns_hashmap_t/ns_lockable_pointer_t.h>
#include <ns_hashmap_t/ns_lockable_wrapper_t.h>
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

namespace ns
{
  template < typename type_t, bool pod=std::is_pod<type_t>::value >
  struct ns_stack_t
  {
    ns_stack_t() :
      head(0)
    , tail(0)
    {
    }

    struct link_t
    {
      typedef ns::ns_pointer_t<link_t> link_ptr ;
      link_t( type_t* rhs ):
        value( rhs )
      , next( nullptr )
      {
      }

      type_t*     value ;
      link_t*     next ;
      ~link_t()
      {
        next = 0 ;
      }
    } ;

    typedef ns::ns_pointer_t<link_t> link_ptr ;

    link_ptr   head ;
    link_ptr   tail ;

    void push( type_t rhs )
    {
      if ( rhs )
      {
        writelock_t lock( head ) ;
        if ( unlikely( ! head ) )
        {
          head = tail = new link_t( new type_t( rhs ) ) ;
        }
        else
        {
          tail->next = new link_t( new type_t( rhs ) ) ;
          tail       = tail->next ;
        }
      }
    }

    type_t* pop()
    {
      writelock_t lock( head ) ;
      link_ptr link( head ) ;
      type_t* value( 0 ) ;
      if ( likely( link ) )
      {
        head = head->next ;
        if ( link->value )
        {
          value = link->value ;
        }
      }
      if ( ! head )
      {
        head = tail = 0 ;
      }
      return value ;
    }
  } ;

  ////////////////////////////////////////////////////////////////////////
  // struct ns_stack_t< ns::lockable_wrapper_t< T >, false >
  ////////////////////////////////////////////////////////////////////////
  template < typename T >
  struct ns_stack_t< ns::lockable_wrapper_t< T >, false >
  {
    ns_stack_t() :
      head(0)
    , tail(0)
    {
    }

    struct link_t
    {
      typedef ns::ns_pointer_t<link_t> link_ptr ;
      typedef lockable_wrapper_t< T > wrapper_t ;

      link_t( wrapper_t&& rhs ):
        value( std::move( rhs ) )
      , next( nullptr )
      {
        rhs.reset() ;
      }

      wrapper_t   value ;
      link_t*     next ;

      ~link_t()
      {
        next = 0 ;
      }
    } ;

    typedef ns::ns_pointer_t<link_t> link_ptr ;
    typedef lockable_wrapper_t< T > wrapper_t ;

    link_ptr   head ;
    link_ptr   tail ;

    void push( wrapper_t&& rhs )
    {
      if ( rhs )
      {
        writelock_t lock( head ) ;
        if ( unlikely( ! head ) )
        {
          head = tail = new link_t( std::move( rhs ) ) ;
        }
        else
        {
          tail->next = new link_t( std::move( rhs ) ) ;
          tail       = tail->next ;
        }
      }
    }

    wrapper_t pop()
    {
      writelock_t lock( head ) ;
      link_ptr link( head ) ;
      if ( likely( link ) )
      {
        head = head->next ;
        if ( link->value )
        {
          return link->value ;
        }
      }
      if ( ! head )
      {
        head = tail = 0 ;
      }
      return 0 ;
    }
  } ;
}
#endif
