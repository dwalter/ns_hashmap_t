/*
  ns_hashmap_t.h :

  non-stop wait-free / weight-less [low fat] locking hashmap designed
  to enable a dual lookup hashmap optimized for no with non-stop
  resize.
      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NS_HASHMAP_H
#define NS_HASHMAP_H
#include <ns_lock_t.h>
#include <ns_lockable_t.h>
#include <ns_lockable_pointer_t.h>
#include <ns_lockable_wrapper_t.h>
#include <ns_hashmap_t/ns_hash.h>
#include <ns_hashmap_t/ns_lockable_pointer_t.h>
#include <ns_hashmap_t/ns_stack_t.h>

namespace ns
{
  template < typename key_t, typename data_t >
  struct ns_hashmap_t
  {
    typedef ns_hashmap_t<key_t,data_t>         map_t ;
    typedef map_t*                          map_ptr ;
    
    typedef ns::hashed_t<key_t>             hashkey_t ;

    typedef hashkey_t                       key_type ;
    typedef hashkey_t*                      key_ptr ;

    typedef data_t                          data_type ;

    typedef lockable_wrapper_t< data_t >       wrapper_t ;
    typedef lockable_wrapper_t< ns_hashmap_t > map_wrapper_t ;
    
    struct link_t
    {
      static uint32_t& instances()
      {
	static uint32_t counter = 0 ;
	return counter ;
      }
      static uint32_t& destroyed()
      {
	static uint32_t counter = 0 ;
	return counter ;
      }
      static uint32_t& deleted()
      {
	static uint32_t counter = 0 ;
	return counter ;
      }

      typedef lockable_pointer_t< data_t > lockable_data_ptr_t ;
      typedef ns_pointer_t< link_t > lockable_link_t ;
      typedef ns_pointer_t<link_t>   link_ptr;

      key_ptr            key ;
      wrapper_t          data ;
      link_ptr           next ;
      void  *            filler ;

      link_t( const key_type& key, const data_type& data ):
        key( new key_type( key ) )
      , data( new data_type( data ) )
      , next( 0 )
      {
      }

      link_t( key_type* key, data_type* data ):
        key( key )
      , data( data )
      , next( 0 )
      {
      }

      link_t( key_type* key, wrapper_t&& data ):
        key( key )
      , data( data )
      , next( 0 )
      {
      }

      ~link_t()
      {
        next = 0 ;
        __sync_fetch_and_add( & deleted(), 1 ) ;
        delete key ;
      }
    } ;

    struct list_t
    {
      typedef lockable_pointer_t< data_t > lockable_data_ptr_t ;
      typedef ns_pointer_t<link_t>        link_ptr;

      ns_stack_t< wrapper_t > stack ;

      link_ptr head ;
      link_ptr tail ;

      list_t():
        head( nullptr )
      , tail( nullptr )
      , inserts(0)
      , removes(0)
      {
      }

      ~list_t()
      {
        while( stack.pop() )
          ;
          
        link_ptr node( head ) ;
        while( node )
        {
          head = head->next ;
          delete node.pointer() ;
          node = head ;
        }
      }

      // insert:
      // insert: key exists, replace data, if not locked delete data
      //                                   if locked push delete list
      // insert: key doesn't exist: create new node and insert at tail.
      void
      insert( const key_type& key, const data_type& data )
      {
        link_ptr link( head ) ;
        bool done( ! link ) ;
        while( ! done )
        {
          // if ( link->item && *link->item->key == key )
          if ( link && *link->key == key )
          {
            if ( link->data )
            {
              if ( link->data.locked ||
		   link->data.references()
		 )
              {
                stack.push( std::move( link->data ) ) ;
              }
              else
              {
		link->data.destroy() ;
              }
            }
            link->data = new data_type( data ) ;
            done = true ;
          }
          else
          {
            if ( ! link )
            {
              done = true ;
            }
            else
            {
              link = link->next ;
              done = ! link ;
            }
          }
        }
        if ( ! link )
        {
          __sync_fetch_and_add( & link_t::instances(), 1 ) ; ;
          link = new link_t( key, data ) ;
          if ( ! head )
          {
            head = tail = link ;
          }
          else
          {
            tail->next = link ;
            tail       = link ;
          }
        }
        __sync_fetch_and_add( &inserts, 1 ) ;
      }

      wrapper_t
      get( const key_type& key )
      {
        link_ptr link( head ) ;
        bool done( false ) ;

        while( ! done )
        {
          if ( link && link->key && *link->key == key )
          {
	    if ( link->data )
	    {
	      return link->data ;
	    }
	    else
	    {
	      return 0 ;
	    }
          }
          else
          {
	    if ( ! link )
	    {
	      done = true ;
	    }
	    else
	    {
	      link = link->next ;     
	      done = ! link ;
	    }
          }
        }
	if ( link && link->data )
	{
	  return link->data ;
	}
	else
	{
	  return 0 ;
	}
      }

      wrapper_t
      remove( const key_type& key )
      {
        link_ptr link( head ) ;
        link_ptr last( head ) ;
        bool done( head ) ;
        while ( ! done )
        {
          if ( link && link->key && *link->key == key )
          {
            __sync_fetch_and_add( &removes, 1 ) ;
            if ( head == link )
            {
              head = head->next ;
              if ( ! head )
              {
                head = tail = 0 ;
              }
            }
            else if ( tail == link )
            {
              tail = last ;
            }
            done = true ;
          }
          else
          {
            last = link ;
            link = link->next ;
            done = ! link ;
          }
        }
	if ( link && link->data )
	{
          __sync_fetch_and_add( & link_t::destroyed(), 1 ) ; ;
	  return link->data ;
	}
	else
	{
	  return nullptr ;
	}
      }

      wrapper_t
      reserve( const key_type& key )
      {
	return get( key ) ;
      }
      size_t inserts ;
      size_t removes ;
    } ;

    typedef ns_pointer_t< list_t >          bucket_type ;
    typedef ns_pointer_t< list_t >*         bucket_ptr ;
    typedef ns_pointer_t<link_t>            link_ptr;
    typedef lockable_pointer_t< data_t > lockable_data_ptr_t ;
    bucket_ptr                              table     ;
    size_t                                  size      ; 
    bool                                    marked    ;
    uint32_t                                bits      ;
    uint32_t                                mask      ;
    size_t                                  contents  ;
    size_t                                  inserts ;
    size_t                                  removes ;
    size_t                                  gets ;
    size_t                                  reserves ;

    ns_hashmap_t( size_t size ) :
      table( new bucket_type[ power_of_two( size ) ] )
    , size( power_of_two( size ) )
    , marked( false ) // marked for destruction in resize operation.
    , bits( power_of_two_bit( size ) )
    , mask( hashmask( bits ) )
    , contents( 0 )
    , inserts(0)
    , removes(0)
    , gets(0)
    , reserves(0)
    {
    }

    ~ns_hashmap_t()
    {
      if ( table )
      {
        bucket_ptr temp( __sync_val_compare_and_swap( &table, table, 0L ) ) ;
        if ( temp )
        {
          for ( uint32_t i = 0 ; i < size ; i++ )
          {
            if ( ! temp[i].locked && !temp[i].references )
            {
              delete reinterpret_cast<list_t*>( temp[i].address ) ;
            }
          }
          delete [] temp ;
        }
      }
    }

    void
    insert( link_ptr& link )
    {
      bucket_type& bucket( offset( link->key ) ) ;
      writelock_t lock( bucket ) ;
      if ( ! bucket )
      {
        bucket = new list_t ;
      }
      bucket->insert( link ) ;
      __sync_fetch_and_add( &inserts, 1 ) ;
    }

    void
    insert( const key_type & key, const data_type & data )
    {
      bucket_type& bucket( offset( key ) ) ;
      writelock_t lock( bucket ) ;
      if ( ! bucket )
      {
        bucket = new list_t ;
      }
      bucket->insert( key, data ) ;
      __sync_fetch_and_add( &inserts, 1 ) ;
    }

    wrapper_t
    get( const key_type & key )
    {
      bucket_type& bucket( offset( key ) ) ;
      writelock_t lock( bucket ) ;
      if ( ! bucket )
      {
        return wrapper_t(0) ;
      }
      else
      {
	__sync_fetch_and_add( &gets, 1 ) ;
	return bucket->get( key ) ;
      }
    }

    wrapper_t
    reserve( const key_type & key )
    {
      bucket_type& bucket( offset( key ) ) ;
      writelock_t lock( bucket ) ;
      __sync_fetch_and_add( &reserves, 1 ) ;
      if ( bucket.head )
      {
        __sync_add_and_fetch( & contents, 1 ) ;
        lockable_data_ptr_t node( bucket.reserve( key ) ) ;
        return node ;
      }
      else
      {
        __sync_add_and_fetch( & contents, 1 ) ;
        lockable_data_ptr_t
          node( new link_t( new key_type( key ), 0 ) ) ;
        bucket.head = bucket.tail = node.node ;
        return node ;
      }
    }
  
    wrapper_t
    remove( const key_type & key )
    {
      bucket_type& bucket( offset( key ) ) ;
      writelock_t lock( bucket ) ;
      if ( ! bucket )
      {
        return wrapper_t(0) ;
      }
      else
      {
	link_ptr node( bucket->head ) ;
	__sync_fetch_and_add( & removes, 1 ) ;
	__sync_sub_and_fetch( & contents, 1 ) ;
	return bucket->remove( key ) ;
      }
    }

    link_ptr
    detach( size_t i )
    {
      bucket_type& bucket( index( i ) ) ;
      writelock_t lock( bucket ) ;
      link_ptr node ;
      if ( bucket )
        node = bucket->head ;
      bucket = 0 ;
      return node ? node : nullptr ;
    }

    inline const link_ptr
    detach_by_key( const key_type& key )
    {
      return detach( offset( key ) ) ;
    }

    static void copy( map_wrapper_t& from, map_wrapper_t& to )
    {
      size_t size = power_of_two( from->size ) ;

      for ( uint32_t i = 0 ; i < size ; i++ )
      {
        const link_ptr list( from->detach( i ) ) ;
        if ( list )
        {
          link_ptr node( list ) ;
          while( node )
          {
            link_ptr next( node->next ) ;
            node->next = 0 ;
            if ( node->key && node->data )
              to->insert( *node->key, *node->data ) ;
            delete node.pointer() ;
            node = next ;
          }
        }
      }
    }

    inline bucket_type&
    offset( const key_type& key ) 
    {
      size_t i( key.hash & mask ) ;
      return table[i] ;
    }

    inline bucket_type&
    index( size_t index ) 
    {
      size_t i( index & mask ) ;
      return table[i] ;
    }

    std::ostream& dump( std::ostream& o )
    {
      size_t size( size ) ;
      for ( uint32_t i = 0 ; i < size ; i++ )
      {
        bucket_type& bucket( index( i ) ) ;
        writelock_t lock( & bucket ) ;
        const link_ptr list( bucket.head ) ;
        if ( list )
        {
          link_ptr node( list ) ;
          while( node && ! node.locked )
          {
            o << " k[" << std::setw( 10 ) ;
            if ( node && node->_key )
              o << *node->_key ;
            else
              o << " " ;
            o << "]d[" << std::setw( 10 ) ;
            if ( node ) 
            {
              if ( node->data->locked )
                o << "locked" ;
              else if ( node->_data )
                o << *node->_data ;
            }
            o << "]" ;
            o << std::endl ;
            link_ptr next( node->_next ) ;
            node = next ;
          }
        }
      }
      o << std::endl ;
      return o ;
    }

    std::ostream& print( std::ostream& o )
    {
      size_t size( size ) ;
      for ( uint32_t i = 0 ; i < size ; i++ )
      {
        // was the node deletable?
        bucket_type& bucket( index( i ) ) ;
        writelock_t lock( bucket ) ;
        const link_ptr& list( bucket.head ) ;
        o << "bucket[" << std::setw( 12 ) << i << "]" ;
        if ( list )
        {
          o << "[" << std::setw( 12 ) << list << "]" ;
          // o << "[" << nodes->_tail << "]" ;
          link_ptr node( list ) ;
          while( node )
          {
            o << " k[" << std::setw( 10 ) ;
            if ( node && node->_key )
              o << *node->_key ;
            else
              o << " " ;
            o << "]d[" << std::setw( 10 ) ;
            if ( node ) 
            {
              if ( node.locked || node->data.locked )
                o << "locked" ;
              else if ( node->data )
                o << *node->data ;
            }
            o << "]" ;
          
            link_ptr next( node->_next ) ;
            node = next ;
          }
        }
        o << std::endl ;
      }
      o << std::endl ;
      return o ;
    }
  } ;
}
#endif
