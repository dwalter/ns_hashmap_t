/*    
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NS_HASH_H
#define NS_HASH_H
#include <ns_hashmap_t/ns_enums.h>
#include <ns_hashmap_t/jenkins_hash.h>
#include <ns_hashmap_t/enable_if.h>
#include <cstring>
#include <cstdlib>
#include <ns_hashmap_t/ns_char_t.h>

#define NO_HASH  typename disable_if< have_hash<T>::value, uint32_t >::type
#define HAS_HASH typename  enable_if< have_hash<T>::value, uint32_t >::type
#define NO_HASH_CHAR_PTR  typename disable_if< have_hash<const char*>::value, uint32_t >::type
#define HAS_HASH_CHAR_PTR typename  enable_if< have_hash<const char*>::value, uint32_t >::type

namespace ns
{
  inline uint32_t jenkins( const uint8_t* text, const size_t length, const uint32_t initial )
  {
    uint32_t u = hashlittle( text, length, initial ) ;
    return u ;
  }

  template<typename M>
  struct have_hash
  {
    template <typename S, uint32_t (S::*)()> struct test{};
    template <typename S> static yes_tag X(test<S, &S::hash> *);
    template <typename S> static no_tag  X(...);
    static const bool value = sizeof(X<M>(0)) == sizeof(yes_tag);
  };

  template <typename T>inline HAS_HASH hashobject( const T& t ) ;
  template <typename T>inline HAS_HASH hashobject( const T* t ) ;

  template <typename T>inline NO_HASH  hashobject( const T* t ) ;
  template <>inline NO_HASH_CHAR_PTR   hashobject( const char* ) ;
  template <typename T>inline NO_HASH  hashobject( const T& t ) ;

  template <typename T>inline HAS_HASH hashobject( const T* t )
  {
    return t ? t->hash() : 0 ;
  }

  template <typename T>inline HAS_HASH hashobject( const T& t )
  {
    return t.hash() ;
  }

  template <>inline NO_HASH_CHAR_PTR  hashobject( const char* t )
  {
    return t ? jenkins( reinterpret_cast<const uint8_t*>( t ), strlen( t ), 0 ) : 0 ;
  }

  template <typename T>inline NO_HASH  hashobject( const T* t )
  {
    return t ? jenkins( reinterpret_cast<const uint8_t*>( t ), sizeof( T ), 0 ) : 0 ;
  }

  template <typename T>inline NO_HASH  hashobject( const T& t )
  {
    return jenkins( reinterpret_cast<const uint8_t*>( &t ), sizeof( T ), 0 ) ;
  }

  template <typename key_t>
  struct hash_t
  {
    const uint32_t   hash ;
    hash_t( const key_t* key ) : hash( hashobject( *key ) ) {}
    hash_t( const key_t& key ) : hash( hashobject(  key ) ) {}
  } ;

  template <>
  struct hash_t<std::string>
  {
    const uint32_t hash ;

    hash_t( const std::string* key ) : hash( hashme(  key ) ) {}
    hash_t( const std::string& key ) : hash( hashme( &key ) ) {}
    const uint32_t hashme( const std::string* rhs ) const
    {
      uint32_t u( 0 ) ;
      if ( rhs )
      {
        u = jenkins( reinterpret_cast<const uint8_t*>( rhs->c_str() ), rhs->length(), 0 ) ;
  	std::cerr << "u [" << u << "]\n";
      }
      return u ;
    }
    
  } ;

  template <>
  struct hash_t<char*>
  {
    const uint32_t hash ;
    hash_t( const char* key ) : hash( hashme(  key ) ) {}
    const uint32_t hashme( const char* rhs ) const
    {
      uint32_t u( 0 ) ;
      if ( rhs )
      {
        u = jenkins( reinterpret_cast<const uint8_t*>( rhs ), strlen( rhs ), 0 ) ;
      }
      return u ;
    }
    
  } ;

  template < int n >
  struct hash_t< ns_char_t<n> >
  {
    const uint32_t hash ;
    hash_t( const ns_char_t<n>& key ) : hash( hashme( key ) ) {}
    const uint32_t hashme( const ns_char_t<n> & rhs ) const
    {
      uint32_t u( 0 ) ;
      u = jenkins( reinterpret_cast<const uint8_t*>( rhs.text ), n, 0 ) ;
      return u ;
    }
  } ;

  template <typename key_t>
  struct hashed_t: hash_t<key_t>
  {
    const key_t key  ;
    hashed_t( const key_t& key ) :
      hash_t<key_t>( key )
    , key( key )
    {
    }
    bool operator==( const hashed_t& rhs ) const
    {
      return this->hash == rhs.hash && key == rhs.key ;
    }
  } ;

  template<>
  struct hashed_t< char* >: hash_t<char*>
  {
    const char* key  ;
    hashed_t( const char* key ) : hash_t<char*>( key ), key( strdup( key ) )
    {
    }
    ~hashed_t()
    {
      delete [] key ;
    }
    bool operator==( const hashed_t& rhs ) const
    {
      return hash == rhs.hash && key == rhs.key ;
    }
  } ;

  template< int n >
  struct hashed_t< ns_char_t<n> >: hash_t< ns_char_t<n> >
  {
    const ns_char_t<n> key  ;
    hashed_t( const ns_char_t<n>& key ) : hash_t< ns_char_t<n> >( key ), key( key )
    {
    }
    ~hashed_t()
    {
    }
    bool operator==( const hashed_t& rhs ) const
    {
      return this->hash == rhs.hash && key == rhs.key ;
    }
  } ;
}
#endif
