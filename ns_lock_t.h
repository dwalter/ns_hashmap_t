/*      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NONSTOP_LOCKS_H__
#define NONSTOP_LOCKS_H__
#define FULL_MEMORY_BARRIER __asm__ __volatile__("" : : : "memory" )
// #define FULL_MEMORY_BARRIER __sync_synchronize()
#include <iostream>
#include <trace.h>
#ifdef USE_FUTEX
#include <linux/futex.h>
#endif
#include <stdexcept>
#include <syscall.h>
#include <unistd.h>
#include <ns_enums.h>
#include <ns_hashmap_t/ns_lockable_t.h>
#include <ns_hashmap_t/enable_if.h>

namespace ns
{
  struct unconfigured: std::exception {} ;

#ifdef USE_FUTEX
  inline int futex( volatile uint64_t* uaddr, int op, uint64_t val, timespec *timeout, uint64_t * uaddr2, uint64_t val3 )
  {
    return syscall(SYS_futex, uaddr, op, val, 0, uaddr2, val3 ) ;
  }
  inline int futex_wait( volatile uint64_t* uaddr, int val )
  {
    timespec sleep = {0, 50} ;
    return futex( uaddr, FUTEX_WAIT, val, &sleep, 0, 0 );
  }
  inline int futex_wake( volatile uint64_t* uaddr, int val )
  {
    timespec sleep = {0, 50} ;
    return futex( uaddr, FUTEX_WAKE, val, &sleep, 0, 0 );
  }
#endif

  template <typename type_t>
  struct ns_pointer_t:  lockable_t
  {
    ns_pointer_t() {}
    ~ns_pointer_t(){}
    ns_pointer_t( lockable_t& lockable ) : lockable_t( lockable ){}
    ns_pointer_t( type_t* lockable ) : lockable_t( lockable ){}

#if __WORDSIZE == 64
    const ns_pointer_t<type_t>& operator=( type_t* rhs )
    {
      address = reinterpret_cast<uint64_t>(rhs) ; return *this ;
    }
#else
    const ns_pointer_t<type_t>& operator=( type_t* rhs )
    {
      address = rhs ; return *this ;
    }
#endif

    type_t* operator->()
    {
      return reinterpret_cast<type_t*>( this->address ) ;
    }
    type_t& operator* ()
    {
      return *reinterpret_cast<type_t*>( this->address ) ;
    }
    type_t* pointer   ()
    {
      return reinterpret_cast<type_t*>( this->address ) ;
    }
    // void reset      () { new ( reinterpret_cast<type_t*>( this->address ) ) type_t ; }
    type_t& operator[]( uint32_t i )
    {
      return reinterpret_cast<type_t*>( this->address )[i] ;
    }
    // static void * operator new   ( size_t size )  ;
    // static void   operator delete( void * p ) ;
  } ;

  template <typename type>
  struct node_t: lockable_t
  {
    lockable_t& lockable ;
    node_t() {}
    ~node_t(){}
    node_t( lockable_t& lockable ) : lockable_t( lockable ){}
    const node_t<type>& operator=( type* rhs ) { address = uint64_t( rhs ) ; return *this ; }
    type* operator->() { return reinterpret_cast<type*>( this->address ) ;  }
    type& operator* () { return *reinterpret_cast<type*>( this->address ) ; }
    type* pointer   () { return reinterpret_cast<type*>( this->address ) ; }
    type& operator[]( uint32_t i )  { return reinterpret_cast<type*>( this->address )[i] ; }
    // static void * operator new   ( size_t size )  ;
    // static void   operator delete( void * p ) ;
  } ;

  struct writelock_t
  {
    lockable_t & lockable ;

    writelock_t( lockable_t& rhs ) : lockable( rhs )
    {
      lockable.lock() ;
    }

    ~writelock_t()
    {
      lockable.unlock() ;
    }
  } ;

  struct readlock_t
  {
    lockable_t & lockable ;
    
    readlock_t( lockable_t& rhs ) :
      lockable( rhs )
    {
      lockable.inc() ;
    }

    ~readlock_t()
    {
      lockable.dec() ;
    }
  } ;

#define HAVE_METHOD( m1 )                                               \
  template<typename M>                                                  \
  struct have_hash                                                      \
  {                                                                     \
    template <typename S, void (S::*)()> struct test{};                 \
    template <typename S> static yes_tag X( test<S, &S::m1> * ) ;       \
    template <typename S> static no_tag  X(...);                        \
    static const bool value = sizeof(X<M>(0)) == sizeof(yes_tag);       \
  } ;

#define HAVE_MEMBER( m )                                                \
  template<typename M>                                                  \
  struct have_hash                                                      \
  {                                                                     \
    template <typename S> struct test{};                                \
    template <typename S> static yes_tag X( __typeof(S::m)* ) ;         \
    template <typename S> static no_tag  X(...);                        \
    static const bool value = sizeof(X<M>(0)) == sizeof(yes_tag);       \
  } ;

#define HAVE_METHODS( m1, m2 )                                          \
  template<typename M>                                                  \
  struct have_## m1 ##_ ## m2                                           \
  {                                                                     \
    template <typename S, void (S::*)(), void (S::*)()> struct test{};  \
    template <typename S> static yes_tag X( test<S, &S::m1, &S::m2> * ); \
    template <typename S> static no_tag  X(...);                        \
    static const bool value = sizeof(X<M>(0)) == sizeof(yes_tag);       \
  } ;

HAVE_METHODS( lock, unlock )

#define HAVE_METHODS_TYPE( T,m1, m2 ) have_## m1 ##_ ## m2<T>

#define ENABLE_HAVE_NAMES(T,m1,m2) \
  typename enable_if< HAVE_METHODS_TYPE(T,m1,m2)::value, void >::type

#define DISABLE_HAVE_NAMES(T,m1,m2) \
  typename disable_if< HAVE_METHODS_TYPE(T,m1,m2)::value, void >::type

#define NO_HASH  typename disable_if< have_hash<T>::value, uint32_t >::type
#define HAS_HASH typename  enable_if< have_hash<T>::value, uint32_t >::type

#define HAS_LOCKS(T)  ENABLE_HAVE_NAMES (T,lock,unlock)
#define NO_LOCKS(T)   DISABLE_HAVE_NAMES(T,lock,unlock)

  template <typename T, typename S = void >
  struct strategy_t
  {
    strategy_t( T* )
    {
    }

    void lock()
    {
      throw ;
    }

    void unlock()
    {
      throw ;
    }
  } ;

  template <typename T>
  struct strategy_t< T, HAS_LOCKS(T) >
  {
    T* t ;
    strategy_t( T* t ): t(t)
    {
    }

    void lock()
    {
      TRACE ;
      if ( t ) t->lock() ;
    }

    void unlock()
    {
      TRACE ;
      if ( t ) t->unlock() ;
    }
  } ;

  template <typename T >
  struct strategy_t< T, NO_LOCKS(T) >
  {
    T* t ;
    strategy_t( T* t ): t(t)
    {
    }

    void lock()
    {
      TRACE ;
      lock( t ) ;
    }

    void unlock()
    {
      TRACE ;
      unlock( t ) ;
    }
    void lock( T* t )
    {
      TRACE ;
      if ( t ) t->nolock() ;
    }
    void unlock( T* t )
    {
      TRACE ;
      if ( t ) t->nounlock() ;
    }
  } ;

  template < typename T, typename strategy_t >
  struct monitor_t
  {
    T* object ;
    strategy_t locker ;
    monitor_t( T* object ) :
      object( object )
    , locker( object )
    {
      FULL_MEMORY_BARRIER ;
      lock() ;
    }

    ~monitor_t()
    {
      FULL_MEMORY_BARRIER ;
      unlock() ;
    }

    void lock()
    {
      if ( object )
      {
        locker.lock() ;
      }
    }

    void unlock()
    {
      if ( object )
      {
        locker.unlock() ;
      }
    }

  private:
    monitor_t( monitor_t& rhs ){}
    const monitor_t& operator=( const monitor_t& ){}
  } ;

}
#endif
