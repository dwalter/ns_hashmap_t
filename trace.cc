/*      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#include <trace.h>
#include <iostream>
#include <iomanip>
using namespace std;
namespace xsthr
{
    Trace::Trace(const char*file, int line, const char*function):
      m_file(file)
    , m_line(line)
    , m_function(function)
    {
        uint32_t width = 3*m_depth;
        cerr << setw(width)
             << ">> "
             << m_file
             << ":" 
             << m_line
             << ":"
             << m_function
             << endl;
        m_depth++;
    }

    Trace::~Trace()
    {
        m_depth--;
        uint32_t width = 3*m_depth;
        cerr << setw(width)
             << "<< "
             << m_file
             << ":"
             << m_line
             << ":"
             << m_function
             << endl;
    }


    uint32_t Trace::m_depth=1;

    TraceReferences::TraceReferences(const char*file, int line, const char*function, int instance, int references):
      Trace(file, line, function)
    , m_instance(instance)
    , m_references(references)
    {
        uint32_t width = 3*m_depth;
        cerr << setw(width)
             << ">> "
             << m_file
             << ":"
             << m_line
             << ":"
             << m_function
             << ":references: "
             << m_references
             << " instance: "
             << m_instance
             << endl;
    }

    TraceReferences::~TraceReferences()
    {
        uint32_t width = 3*m_depth;
        cerr << setw(width)
             << "<< "
             << m_file
             << ":"
             << m_line
             << ":"
             << m_function
             << ":references: "
             << m_references
             << " instance: "
             << m_instance
             << endl;
    }
}
