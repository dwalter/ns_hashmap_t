#ifndef ENABLE_IF_H_
#define ENABLE_IF_H_

template <bool, typename T = void> struct enable_if             {};
template <typename T>              struct enable_if  <true,  T> { typedef T type; };
template <bool, typename T = void> struct disable_if            { typedef T type; };
template <typename T>              struct disable_if <true,  T> {} ;
typedef char (&no_tag) [1];
typedef char (&yes_tag) [2];

#endif
