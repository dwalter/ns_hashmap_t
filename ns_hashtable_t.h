/*
  ns_hashmap_t.h :

  non-stop wait-free / weight-less [low fat] locking hashmap designed
  to enable a dual lookup hashmap optimized for no with non-stop
  resize.
      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NS_HASHTABLE_H
#define NS_HASHTABLE_H
#define MIN_TABLE_SIZE 1
#include <ns_hashmap_t.h>
#include <time.h>
bool debugging( false ) ;
namespace ns
{
  template < typename key_t, typename data_t >
  struct ns_hashtable_t
  {
    static timespec const * const delay()
    {
      static timespec delay { 0, 1000 } ;
      return & delay ;
    }
    typedef typename ns_hashmap_t<key_t,data_t>::map_t map_t ;
    typedef typename map_t::wrapper_t                  wrapper_t ;

    typedef typename map_t::key_type                   key_type ;
    typedef typename map_t::key_ptr                    key_ptr ;

    typedef typename map_t::data_type                  data_type ;

    typedef lockable_wrapper_t< map_t >                map_wrapper_t ;

    struct ns_exiting_t : std::exception
    {
    } ;

    // static void* start( void* arg )
    // {
    //   bool done = false ;
    //   while( ! done )
    //   {
    //   }
    //   return 0 ;
    // }

    map_wrapper_t    	foreground ;
    map_wrapper_t       background ;
    bool       	     	resizing ;
    bool                exiting  ;
    // size_t active ;
    // struct active_t
    // {
    //   active_t()
    //   {
    //     __sync_add_and_fetch( &active, 1 ) ;
    //   }

    //   ~active_t()
    //   {
    //     __sync_sub_and_fetch( &active, 1 ) ;
    //   }

    //   operator bool()()
    //   {
    //     FULL_MEMORY_BARRIER ;
    //     return active > 0 ;
    //   }
    // } ;

    typedef std::pair< map_wrapper_t, map_wrapper_t >  map_pair ;

/*

  1. initialize maps [ map_pair_ptr ] to a valid pointer to a map
  pair.

  2. initialize first to a valid pointer in map_pair_ptr

  3. pre condition first is always a valid pointer

  4. pre condition second hashmap can be either write-able or marked
  for destruction, effectively for read-only access.

  5. read only operations can be performed while marked is set

*/

    size_t                                  inserts ;
    size_t                                  removes ;
    size_t                                  gets ;
    size_t                                  reserves ;
    size_t                                  token ;
    ns_hashtable_t( size_t size ):
      resizing( false )
    , exiting( false )
    , inserts(0)
    , removes(0)
    , gets(0)
    , reserves(0)
    , token( 0 )
    {
      foreground = new map_t( size ) ;
    }

    ~ns_hashtable_t()
    {
    }

    struct block_t
    {
      block_t()
      {
        while( ! __sync_bool_compare_and_swap( & resizing, false, true ) && ! exiting )
          nanosleep( delay(), 0 );
        if ( exiting )
          throw ns_exiting_t() ;
      }

      ~block_t()
      {
        while( ! __sync_bool_compare_and_swap( & resizing, true, false ) )
          ;
        if ( exiting )
          throw ns_exiting_t() ;
      }
    } ;

    void
    resize()
    {
      // size_t total ( 0 ) ;
      // size_t items ( 0 ) ;
      size_t size  ( 0 ) ;
      bool   grow  ( false ) ;
      bool   shrink( false ) ;
      FULL_MEMORY_BARRIER ;
      if ( ! resizing )
      {
        // total = foreground->contents ;
        size =  foreground->size ;
        grow =  foreground->contents * 1.5 > size ;
        shrink =
          size > MIN_TABLE_SIZE * 2 && foreground->contents < size * .2 ;
        if ( grow || shrink )
        {
          try
          {
            FULL_MEMORY_BARRIER ;
            while( background.references() > 1 )
              FULL_MEMORY_BARRIER ;
            //////////////////////////////////////////////////
            // background release locks and pointer.
            //////////////////////////////////////////////////
            background.swap( map_wrapper_t() ) ;
            map_t* next( new map_t( size ) ) ;
            // take the lock with the item
            background.lockable = foreground.lockable ;
            background.locked = true ;
            // foreground is always the active insertion map
            lockable_t* x( (lockable_t*)next ) ;
            lockable_t* f = foreground.lockable ;

            x->locked     = f->locked ;
            x->references = f->references ;
            x->malloc     = f->malloc ;

            while( ! __sync_bool_compare_and_swap( & foreground.lockable
                                                 ,   foreground.lockable
                                                 ,   x
                                                 )
                 )
              ; // empty loop

            FULL_MEMORY_BARRIER ;
            if ( background && foreground )
            {
              block_t block() ;
              map_t::copy( background, foreground ) ;
            }
            // background has no extra references
            FULL_MEMORY_BARRIER ;
            while( background.references() > 1 )
              FULL_MEMORY_BARRIER ;
            background.swap( map_wrapper_t() ) ;
          }
          catch( ns_exiting_t & e )
          {
          }
        }
      }
    }

    wrapper_t
    get( const key_type& key )
    {
      resize() ;
      wrapper_t rc ;
      try
      {
        map_wrapper_t temp( foreground ) ;
        rc = temp->get( key ) ;
        if ( ! rc && resizing )
          rc = background->get( key ) ;
      }
      catch( std::exception & e )
      {
        std::cerr << e.what() << std::endl ;
      }
      return rc ;
    }

    void
    insert( const key_type & key, const data_type & data )
    {
      resize() ;
      try
      {
        foreground->insert( key, data ) ;
        __sync_fetch_and_add( &inserts, 1 ) ;
      }
      catch( std::exception & e )
      {
        std::cerr << e.what() << std::endl ;
      }
    }

    wrapper_t
    remove( const key_type & key )
    {
      resize() ;
      wrapper_t rc ;
      try
      {
        FULL_MEMORY_BARRIER ;
        size_t token( this->token ) ;
        map_wrapper_t temp( foreground ) ;
        if ( ! temp )
        {          
          rc = temp->remove( key ) ;
          FULL_MEMORY_BARRIER ;
          if ( ! rc && token != this->token )
          {
            temp = background ;
            if ( ! rc && temp )
            {
              rc = temp->remove( key ) ;
            }
          }
        }
      }
      catch( std::exception & e )
      {
        std::cerr << e.what() << std::endl ;
      }
      return rc ;
    }

    size_t size()
    {
      map_wrapper_t f( foreground ) ;
      map_wrapper_t b( background ) ;
      size_t contents = b ? b->contents : 0 ;
      return f && f != b ? f->contents + contents : contents ;
    }

    inline
    std::ostream& print( std::ostream& o )
    {
      try
      {
        block_t block() ;
        return foreground->print( o ) ;
      }
      catch( const ns_exiting_t & e )
      {
      }
      return o ;
    }

    inline
    std::ostream& dump( std::ostream& o )
    {
      try
      {
        block_t block() ;
        return foreground->dump( o ) ;
      }
      catch( const ns_exiting_t & e )
      {
      }
      return o ;
    }

    friend std::ostream& operator<<( std::ostream& o, map_t& rhs )
    {
      return rhs.print( o ) ;
    }
  } ;
}

#endif
