/*      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef NS_ENUMS_H
#define NS_ENUMS_H

namespace ns
{
  enum LOCK_CONSTANTS
  {
    MAX_PARALLEL                = 4LL,
    MAX_PARALLEL_MASK           = MAX_PARALLEL - 1 ,
    MINIMUM_BUCKET_ENTRIES      = (uint64_t)( 1LL << 20 ),
    MAX_ARENAS                  = 1 ,
    BITS_IN_ADDRESS             = 52LL , // Currently AMD is 48 bits may extend to 52
    ADDRESS_MASK                = ( ( 1LL<<BITS_IN_ADDRESS ) - 1 ) ,
    NOT_ADDRESS_MASK            = ~( ( 1LL<<BITS_IN_ADDRESS ) - 1 ) ,
    MAX_REFERENCES              = (( 1 << 9 ) - 1),
    RESERVE_BIT_MASK            = ( 1LL<< 61 ) ,
    RESERVE_BIT                 = 61 ,
    MALLOC_BIT_MASK             = ( 1LL<< 62 ) ,
    MALLOC_BIT                  = 62 ,
    LOCK_BIT                    = 63 ,
    LOCK_BIT_MASK               = ( 1LL<< 63 ) ,
    REFCOUNT_MASK               = ~( LOCK_BIT_MASK | RESERVE_BIT_MASK | MALLOC_BIT_MASK | ADDRESS_MASK ) ,
    REFCOUNT_SHIFT              = BITS_IN_ADDRESS
  } ;

  enum elock
  {
    NONE = 0,
    USE_TEST_AND_SET,
    USE_MINUS_ONE,
    // USE_LOCK,
    USE_CAS,
    USE_CAS_COUNTABLE,
    USE_PTMUTEX
  } ;
}
#endif
