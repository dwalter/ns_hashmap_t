#!/bin/bash
function version
{
    if [[ ! -e .version ]]; then
	v=00.00
    else
	v=$(cat .version)
    fi
    local all=${v};
    local minor=${all#*.};
    local major=${all%.*}
    major=${major#0*}
    minor=${minor#0*}
    minor=$(( minor+1 ))
    if (( minor > 99 )); then
	minor=0;
	major=$(( major+1 )) ;
    fi
    version=$(printf "%02d.%02d\n" ${major} ${minor})
    printf "%02d.%02d\n" ${major} ${minor} > .version
}
version