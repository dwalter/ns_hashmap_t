#ifndef NS_CHAR_T_H__
#define NS_CHAR_T_H__
#include <iostream>
#include <iomanip>
#include <stdint.h>
#include <stdexcept>
#include <cstdlib>
#include <cstring>

namespace ns
{
  template < uint32_t n >
  struct ns_char_t
  {
    static uint32_t& instances()
    {
      static uint32_t counter = 0 ;
      return counter ;
    }

    static uint32_t& destroyed()
    {
      static uint32_t counter = 0 ;
      return counter ;
    }

    static uint32_t& copy_constructed()
    {
      static uint32_t counter = 0 ;
      return counter ;
    }

    static constexpr uint32_t size = n ;
    // round up to the next word.
    static constexpr uint32_t bytes = n%4 == 0 ? n + 4 : n + (4 - n%4) ;

    char text[ bytes ] ;

    ns_char_t( const char* rhs )
    {
      __sync_fetch_and_add( & instances(), 1 ) ;
      uint32_t i = rhs ? strlen( rhs ) : 0 ;
      copy( text, rhs, ( i < n ? i : n ) ) ;
      clear( i ) ;
    }
    ns_char_t( const std::string& rhs )
    {
      __sync_fetch_and_add( & instances(), 1 ) ;
      uint32_t i = rhs.length() ;
      copy( text, rhs.c_str(), ( i < n ? i : n ) ) ;
      clear( i ) ;
    }
    ns_char_t( const ns_char_t<n>&& rhs )
    {
      __sync_fetch_and_add( & copy_constructed(), 1 ) ;
      copy( text, rhs.text, n ) ;
    }
    ns_char_t( const ns_char_t<n>& rhs )
    {
      __sync_fetch_and_add( & copy_constructed(), 1 ) ;
      copy( text, rhs.text, n ) ;
    }
    template< int u >
    ns_char_t( const ns_char_t<u>& rhs )
    {
      __sync_fetch_and_add( & instances(), 1 ) ;
      copy( text, rhs.text, ( u < n ? u : n ) ) ;
      clear( u ) ;
    }

    ns_char_t()
    {
      __sync_fetch_and_add( & instances(), 1 ) ;
      initialize( text, n ) ;
      text[n] = 0 ;
    }
    ~ns_char_t()
    {
      __sync_fetch_and_add( & destroyed(), 1 ) ;
    }
    const char* const c_str() const
    {
      return text ;
    }
    void copy( char* lhs, const char* rhs, size_t size )
    {
      if ( size )
      {
        switch( size )
        {
        case 1:
        case 2:
          *(uint16_t*)lhs = *(uint16_t*)rhs ;
          break ;
        case 3:
        case 4:
          *(uint32_t*)lhs = *(uint32_t*)rhs ;
          break ;
        case 5:
        case 6:
          *(uint32_t*)lhs = *(uint32_t*)rhs ; 
          *(uint16_t*)(lhs+4) = *(uint16_t*)(rhs+4) ;
          break ;
        case 7:
        case 8:
          *(uint64_t*)lhs = *(uint64_t*)rhs ;
          break ;
        default:
          *(uint64_t*)lhs = *(uint64_t*)rhs ;
          copy( lhs + 8, rhs + 8, size - 8 ) ;
          break ;
        }
      }
    }

    void initialize( char* lhs, size_t size )
    {
      if ( size )
      {
        switch( size )
        {
        case 1:
        case 2:
          *(uint16_t*)lhs = (uint16_t)0 ;
          break ;
        case 3:
        case 4:
          *(uint32_t*)lhs = (uint32_t)0 ;
          break ;
        case 5:
        case 6:
          *(uint32_t*)lhs = (uint32_t)0 ; 
          *(uint16_t*)(lhs+4) = (uint16_t)0 ;
          break ;
        case 7:
        case 8:
          *(uint64_t*)lhs = (uint64_t)0 ;
          break ;
        default:
          *(uint64_t*)lhs = (uint64_t)0 ;
          initialize( lhs + 8, size - 8 ) ;
          break ;
        }
      }
    }
    
    const ns_char_t<n>& operator=( const char* rhs )
    {
      int i = rhs ? strlen( rhs ) : 0 ;
      memcpy( text, rhs, ( i < n ? i : n ) ) ;
      clear( i ) ;
      return *this ;
    }
    const ns_char_t<n>& operator=( const ns_char_t<n>& rhs )
    {
      memcpy( text, rhs.text, n ) ;
      return *this ;
    }
    template< uint32_t u >
    const ns_char_t<n>& operator=( const ns_char_t< u >& rhs )
    {
      memcpy( text, rhs.text, ( u < n ? u : n) ) ;
      clear( u ) ;
      return *this ;
    }
    void clear( uint32_t i )
    {
      if ( i <= bytes )
      {
	memset( &text[i], 0, bytes - i ) ;
      }
    }
    char& operator[]( size_t i )
    {
      if ( i >= size ) throw std::out_of_range( "ns_char_t<>" ) ;
      return text[i] ;
    }
    bool operator==( const ns_char_t<n>& rhs ) const
    {
      return strncmp( text, rhs.text, n ) == 0 ;
    }
    bool operator<( const ns_char_t<n>& rhs ) const
    {
      return strncmp( text, rhs.text, n ) < 0 ;
    }
    bool operator>( const ns_char_t<n>& rhs ) const
    {
      return strncmp( text, rhs.text, n ) > 0 ;
    }
    template <uint32_t u> bool operator==( const ns_char_t<u>& rhs ) const
    {
      return strncmp( text, rhs.text, ( u <= n ? u : n ) ) == 0 ;
    }
    template <uint32_t u> bool operator<( const ns_char_t<u>& rhs ) const
    {
      return strncmp( text, rhs.text, ( u <= n ? u : n ) ) < 0 ;
    }
    template <uint32_t u> bool operator>( const ns_char_t<u>& rhs ) const
    {
      return strncmp( text, rhs.text, ( u <= n ? u : n ) ) > 0 ;
    }
    friend std::ostream& operator<<( std::ostream& o, ns_char_t<n>& rhs )
    {
      return o << rhs.c_str() ;
    }
  } ;
  template <> struct ns_char_t<0> ;
}

#endif
