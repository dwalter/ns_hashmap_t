/*      
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/
#ifndef NS_LOCKABLE_T_H
#define NS_LOCKABLE_T_H
#define FULL_MEMORY_BARRIER __asm__ __volatile__("" : : : "memory" )
// #define FULL_MEMORY_BARRIER __sync_synchronize()

#include <iostream>
#ifdef USE_FUTEX
#include <linux/futex.h>
#endif
#include <stdexcept>
#include <syscall.h>
#include <unistd.h>
#include <limits.h>
#include <cstring>

namespace ns
{
  struct lockable_t
  {
    lockable_t( void* rhs )
    {
      memset( this, 0, sizeof( lockable_t ) ) ;
      object = rhs ;
    }

    lockable_t( const lockable_t& rhs )
    {
      memcpy( this, &rhs, sizeof( lockable_t ) ) ;
    }

    lockable_t( const lockable_t&& rhs )
    {
      memcpy( this, &rhs, sizeof( lockable_t ) ) ;
    }

    const lockable_t& operator=( const lockable_t&& rhs )
    {
      memcpy( this, &rhs, sizeof( lockable_t ) ) ;
      return *this ;
    }

    const lockable_t& operator=( const lockable_t& rhs )
    {
      memcpy( this, &rhs, sizeof( lockable_t ) ) ;
      return *this ;
    }

    lockable_t()
    {
      memset( this, 0, sizeof( *this ) ) ;
    }

#if __WORDSIZE == 64
#ifdef TWO_64_BIT_SLOTS
    struct
    {
      union
      {
	void* object ;
	uint64_t address ;
      } ;
      union
      {
	struct
	{
	  uint64_t references : 61 ;
	  uint64_t reserved   :  1 ;
	  uint64_t malloc     :  1 ;
	  uint64_t locked     :  1 ;
	} ;
	uint64_t lock_bits ;
	uint64_t high_order_address ;
      } ;
    } ;
#else
    union
    {
      struct
      {
        uint64_t address    : 52 ;
        uint64_t unused     : 12 ;
      } ;
      struct
      {
        uint32_t low_address_bits ;
        union
        {
          uint32_t high_order_address ;
          struct
          {
            uint64_t ignore_bits: 20 ;
            uint64_t references :  9 ;
            uint64_t reserved   :  1 ;
            uint64_t malloc     :  1 ;
            uint64_t locked     :  1 ;
          } ;
        } ;
      } ;
      void    *object ;
      uint64_t text ;
    } ;
#endif
#else
    struct
    {
      union
      {
	void* object ;
	void* address ;
      } ;
      union
      {
	struct
	{
	  uint32_t references : 29 ;
	  uint32_t reserved   :  1 ;
	  uint32_t malloc     :  1 ;
	  uint32_t locked     :  1 ;
	} ;
	uint32_t lock_bits ;
      } ;
    } ;
#endif

    bool operator!() const  { return address == 0 ; }
    bool operator()()const  { return address != 0 ; }
    operator bool()  const  { return address != 0 ; }
    bool malloced()  const  { return malloc ;       }
    void mark_malloced()    { malloc = 1 ;          }

    inline void lock()
    {
      bool done = false ;
      while( ! done )
      {
	lockable_t lock( *this ) ;
	lock.references = 0 ;
	lock.locked = 0 ;
	lockable_t current( lock ) ;
	lock.locked = 1 ;
#if __WORDSIZE == 64
	done =
	  __sync_bool_compare_and_swap( &high_order_address
				      ,  current.high_order_address
				      ,  lock.high_order_address ) ;
#else
	done =
	  __sync_bool_compare_and_swap( &this->lock_bits
				      ,  current.lock_bits
				      ,  lock.lock_bits ) ;
#endif	  
      }
    }

    inline void unlock()
    {
      FULL_MEMORY_BARRIER ;
      this->locked = 0 ;
    }

    inline void add( int32_t i )
    {
      bool done = false ;
      while( ! done )
      {
	lockable_t lock( *this ) ;
	lockable_t current( lock ) ;
	lock.references += i ;
	lock.locked = 0 ;
#if __WORDSIZE == 64
	done =
	  __sync_bool_compare_and_swap( &high_order_address
				      ,  current.high_order_address
				      ,  lock.high_order_address ) ;
#else
	done =
	  __sync_bool_compare_and_swap( &this->lock_bits
				      ,  current.lock_bits
				      ,  lock.lock_bits ) ;
#endif	  
      }
    }

    inline void inc()
    {
      add( 1 ) ;
    }
    inline void dec()
    {
      add( -1 ) ;
    }

  } ;
}

#include <ns_hashmap_t/ns_lockable_pointer_t.h>
#endif
