/*    
  Copyright (C) 2012 David Walter<walter.david@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

#ifndef __TRACE__H__
#define __TRACE__H__
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <inttypes.h>
namespace xsthr
{
    struct Trace
    {
        static uint32_t m_depth;
        const char*   m_file;
        int           m_line;
        const char*   m_function;
        Trace(const char*file, int line, const char*function);
        virtual ~Trace();
    };

    struct TraceReferences: public Trace
    {
        int m_instance;
        int m_references;
        TraceReferences(const char*file, int line, const char*function, int instance, int references);
        virtual ~TraceReferences();
    };
}
// #undef UNTRACE

#ifdef UNTRACE
    #ifdef TRACE
        #undef TRACE
        #undef TRACE_INSTANCE
    #endif
//     #warning Untracing
    #define TRACE
    #define TRACE_INSTANCE
#else
    #ifdef TRACE
        #undef TRACE
        #undef TRACE_INSTANCE
    #endif
//     #warning Tracing
#define TRACE xsthr::Trace Trace ## __FILE__ ## __FUNCTION__(__FILE__, __LINE__, __FUNCTION__)
#define TRACE_INSTANCE    xsthr::TraceReferences TraceReferences ## __FILE__ ## __FUNCTION__(__FILE__, __LINE__, __FUNCTION__, instance_, referencem_)
#endif

#endif

// local variables:
// mode: c++
// end:
